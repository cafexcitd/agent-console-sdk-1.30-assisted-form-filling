<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://192.168.0.99:8443/assistserver/sdk/web/agent/css/assist-console.css">
    <link rel="stylesheet" href="https://192.168.0.99:8443/assistserver/sdk/web/shared/css/shared-window.css">

    <style type="text/css">

        #local {
            width: 160px;
            height: 120px;
        }

        #remote {
            width: 320px;
            height: 240px;
        }

        #local, #remote, #share, #caller-form {
            border: 1px solid grey;
        }

        #share {
            width: 1080px;
            height: 640px;
            position: relative;
        }

        #caller-form {
            width: 640px;
            height: 480px;
        }

        #quality {
            height: 40px;
            width: 100px;
        }

        #quality.call-quality-good {
            background-color: green;
        }

        #quality.call-quality-moderate {
            background-color: yellow;
        }

        #quality.call-quality-poor {
            background-color: red;
        }

    </style>
</head>

<body>

<h3>Screen share</h3>
<!-- remote screen share -->
<div id="share">

</div><!-- request the consumer allow screen share -->
<button id="request-share">Request screen share</button>

<h3>Mode</h3>
<form class="mode">
    <input type="radio" name="mode" value="control" checked>Control<br>
    <input type="radio" name="mode" value="draw">Draw<br>
    <input type="radio" name="mode" value="spotlight">Spotlight<br>
</form>

<h3>Annotation Control</h3>
<form class="annotation-control">
    <input type="color" class="color" value="#ff0000">
    <input type="text" class="opacity" value="0.5">
    <input type="text" class="width" value="2">
    <button type="submit">Update</button>
    <button id="clear">Clear</button>
</form>

<h3>Video</h3>

<!-- remote video view -->
<div id="remote"></div>

<!-- local video preview -->
<div id="local"></div>

<!-- button through which agent clicks to end call -->
<button id="end">End</button>

<!-- indicates the quality of the call -->
<div id="quality">Call Quality</div>

<h3>Push</h3>
<form class="push">
    <input type="text" class="url" placeholder="URL"><br>
    <input type="radio" name="type" value="document" checked>Document<br>
    <input type="radio" name="type" value="content">Content<br>
    <input type="radio" name="type" value="link">Link<br>
    <button type="submit">Push</button>
</form>

<!-- button through which agent closes remote shared doc -->
<button id="closeSharedDoc">Close doc</button>


<h3>Assisted form completion</h3>
<!-- remote screen share -->
<div id="shared-form-container">
    <div id="child-element-to-be-replaced"></div>
</div>


<!-- libraries needed for Assist SDK -->
<script src="http://192.168.0.99:8080/assistserver/sdk/web/shared/js/thirdparty/i18next-1.7.4.min.js"></script>
<script src="http://192.168.0.99:8080/gateway/adapter.js"></script>
<script src="http://192.168.0.99:8080/gateway/csdk-sdk.js"></script>
<script src="http://192.168.0.99:8080/assistserver/sdk/web/shared/js/assist-aed.js"></script>
<script src="http://192.168.0.99:8080/assistserver/sdk/web/shared/js/shared-windows.js"></script>
<script src="http://192.168.0.99:8080/assistserver/sdk/web/agent/js/assist-console.js"></script>
<script src="http://192.168.0.99:8080/assistserver/sdk/web/agent/js/assist-console-callmanager.js"></script>



<!-- load jQuery - helpful for DOM manipulation -->
<script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>

<!-- control -->
<script>

    <?php
    // since we know that the agent will require to be provisioned, we
    // can eagerly provision a session token on the page load rather
    // than do so lazily later

    // include the Provisioner class we created to do the provisioning
    require_once('Provisioner.php');
    $provisioner = new Provisioner;
    $token = $provisioner->provisionAgent(
        'agent1',
        '192.168.0.99',
        '.*');

    // decode the sessionID from the provisioning token
    $sessionId = json_decode($token)->sessionid;
    ?>

    var sessionId = '<?php echo $sessionId; ?>';
    console.log("SessionID: " + sessionId);

    var AgentModule = function (sessionId) {
        // Declare variables needed during the call
        var
        // native DOM elements
            remote,
            local,
            quality,
            share,
            sharedDocId,
            sharedFormContainer,
        // jQuery elements for easier event binding
            $end,
            $requestShare,
            $push,
            $closeSharedDoc,
            $mode,
            $annotationControl;


        // organise functions to do important tasks...
        cacheDom(); // cache the DOM elements we will need later during the call
        setClickHandlers();  // define what happens when our app's buttons are clicked
        bindAgentSdkCallbacks();  // set all of the agentSdk's call-back listeners
        linkUi(); //
        // ...then, with everything prepared and ready ...
        init(sessionId);  // init the library and register the agent to receive calls


        // caches the DOM elements we will need later during the call
        function cacheDom() {
            // extract native DOM elements - AgentSDK works with DOM elements directly
            remote = $('#remote')[0];
            local = $('#local')[0];
            quality = $('#quality')[0];
            share = $('#share')[0];
            sharedFormContainer = $('#shared-form-container')[0];
            // cache as jQuery objects for easier UI event handling
            $requestShare = $('#request-share');
            $end = $('#end');
            $push = $('.push');
            $closeSharedDoc = $('#closeSharedDoc');
            $mode = $('.mode');
            $annotationControl = $('.annotation-control');
        }

        // add click handlers that determine what happens when agent clicks buttons, etc
        function setClickHandlers() {
            // handle end call button being clicked
            $end.click(function(event) {
                // clear the remove video view - optional
                $(remote).find('video').attr('src', '');
                // end the call
                CallManager.endCall();
            });

            // handle request screen share button being clicked
            $requestShare.click(function (event) {
                AssistAgentSDK.requestScreenShare();
            });

            // handle document push button being clicked
            $push.submit(function (event) {
                // prevent the normal form submission behaviour
                event.preventDefault();

                var $this = $(this);
                var url = $this.find('.url').val();
                var type = $this.find('[name=type]:checked').val();

                var method, callback;
                switch(type) {
                    case 'content':
                        AssistAgentSDK.pushContent(url, function(completionEvent) {
                            sharedDocId = completionEvent.sharedDocId;
                            console.log('------------ myPushContentCallBack. Doc ID: ' + sharedDocId);
                        });
                        break;
                    case 'document':
                        AssistAgentSDK.pushDocument(url, function(completionEvent) {
                            sharedDocId = completionEvent.sharedDocId;
                            console.log('------------ myPushDocumentCallBack. Doc ID: ' + sharedDocId);
                        });
                        break;
                    case 'link':
                        AssistAgentSDK.pushLink(url);
                        break;
                }
            });

            // handle close shared document button being clicked
            $closeSharedDoc.click(function (event) {
                console.log('------- closing remote doc. ID: ' + sharedDocId );
                AssistAgentSDK.closeDocument(sharedDocId);
            });

            // handle the change mode button being clicked
            $mode.change(function () {
                var $this = $(this);
                var mode = $this.find('[name=mode]:checked').val();
                console.log( '----------- Mode: ' + mode );
                // determine which mode to switch in to
                switch(mode) {
                    case 'control':
                        console.log('--------- controlSelected');
                        AssistAgentSDK.controlSelected();
                        break;
                    case 'draw':
                        console.log('--------- drawSelected');
                        AssistAgentSDK.drawSelected();
                        break;
                    case 'spotlight':
                        console.log('--------- spotlightSelected');
                        AssistAgentSDK.spotlightSelected();
                        break;
                }
            });

            // handle the annocation controls being set
            $annotationControl.submit(function (event) {
                // prevent normal form submission behaviour
                event.preventDefault();

                // extract the vars
                var $this = $(this);
                var color = $this.find('.color').val();
                var opacity = $this.find('.opacity').val();
                var width = $this.find('.width').val();

                // update the annotation draw style
                AssistAgentSDK.setAgentDrawStyle(color, opacity, width);
            });
        }

        // set all of the agentSdk's call-back listeners
        function bindAgentSdkCallbacks() {
            // Connection events
            AssistAgentSDK.setConnectionEstablishedCallback(function () {
                console.log('----------- setConnectionEstablishedCallback');
            });

            AssistAgentSDK.setConnectionLostCallback(function () {
                console.log('----------- setConnectionLostCallback');
            });

            AssistAgentSDK.setConnectionReestablishedCallback(function () {
                console.log('----------- setConnectionReestablishedCallback');
            });

            AssistAgentSDK.setConnectionRetryCallback(function (retryCount, retryTimeInMilliSeconds) {
                console.log('----------- setConnectionRetryCallback');
            });

            // Call event
            AssistAgentSDK.setConsumerEndedSupportCallback(function () {
                console.log('----------- setConsumerEndedSupportCallback');
                // clear the remove video view - optional
                $(remote).find('video').attr('src', '');
            });

            // Assist feature events
            AssistAgentSDK.setFormCallBack(function (sharedForm) {
                console.log('---------- setFormCallBack');
                sharedFormContainer.replaceChild(sharedForm, sharedFormContainer.children[0]);
            });

            AssistAgentSDK.setRemoteViewCallBack(function (x, y) {
                console.log('------------- setRemoteViewCallBack');
                console.log('x: ' + x + ', y: ' + y);
                // the div where the screen share is shown
                var $share = $('#share');

                var containerHeight = $share.height();
                var containerWidth = $share.width();
                var containerAspect = containerHeight / containerWidth;
                var remoteAspect = y / x;

                var height;
                var width;
                if (containerAspect < remoteAspect) {
                    // Container aspect is taller than the remote view aspect
                    height = Math.min(y, containerHeight);
                    width = height * (x / y);
                } else {
                    // Container aspect is wider than (or the same as) the remote view aspect
                    width = Math.min(x, containerWidth);
                    height = width * (y / x);
                }
                $share.height(height).width(width);
            });

            AssistAgentSDK.setScreenShareActiveCallback(function (active) {
                console.log('------------ ssetScreenShareActiveCallback active: ' + active);
            });

            AssistAgentSDK.setScreenShareRejectedCallback(function () {
                console.log('------------ setScreenShareRejectedCallback. Caller has rejected agents invitation to screen share');
            });

            AssistAgentSDK.setSnapshotCallBack(function (snapshot) {
                console.log('------------ setSnapshotCallBack');
            });
        }


        // links UI elements to their Agent SDK outlets
        function linkUi() {
            // video calling elements
            CallManager.setRemoteVideoElement(remote);
            CallManager.setLocalVideoElement(local);
            CallManager.setCallQualityIndicator(quality);

            // Set screen share element
            AssistAgentSDK.setRemoteView(share);
        }

        // initialises using the Agent SDK CallManager
        function init(sessionId) {

            var gatewayUrl = 'http://192.168.0.99:8080';
            CallManager.init({
                sessionToken: sessionId,
                autoanswer: false,
                username: 'agent1',
                password: 'none',
                agentName: 'Agent',
                agentPictureUrl: gatewayUrl + '/assistserver/img/avatar.png',
                url: gatewayUrl
            });
        }

    }(sessionId);

</script>
</body>
</html>







